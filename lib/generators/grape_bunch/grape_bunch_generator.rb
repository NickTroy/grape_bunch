require 'rails/generators/named_base'

module GrapeBunch
  module Generators
    class GrapeBunchGenerator < Rails::Generators::NamedBase
      include Rails::Generators::ResourceHelpers

      namespace "grape_bunch"
      source_root File.expand_path("../templates", __FILE__)

      desc "Generates a model with the given NAME (if one does not exist) with grape_bunch " <<
           "configuration plus a migration file." #and grape_bunch routes."

      hook_for :orm

      # def add_grape_bunch_routes
      #   grape_bunch_route  = "grape_bunch_for :#{plural_name}"
      #   grape_bunch_route << %Q(, class_name: "#{class_name}") if class_name.include?("::")
      #   grape_bunch_route << %Q(, skip: :all) unless options.routes?
      #   route grape_bunch_route
      # end
    end
  end
end
